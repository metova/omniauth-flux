# OmniAuth Flux Strategy

This strategy provides authentication with Metova FluxID.

You will need to get an API Key at [https://id.fluxhq.io/oauth/applications](https://id.fluxhq.io/oauth/applications). You will need the Application ID and Secret.

## Installation

Add to your `Gemfile`:

```ruby
gem 'omniauth-flux', git: 'git@bitbucket.org:metova/omniauth-flux.git', branch: 'master'
```

Then `bundle install`.

You should be using `figaro` to manage your secrets. Assuming that is the case, add to your `config/application.yml':

```yaml
FLUX_APP_ID: <your app id>
FLUX_APP_SECRET: <your app secret>
```

**Note:** `config/application.yml` should be in your `.gitignore` so that you do not accidentally commit your secrets to git!

## Usage

### OmniAuth Alone

In `config/initializers/omniauth.rb`:

```ruby
Rails.application.config.middleware.use OmniAuth::Builder do
  provider :flux, ENV['FLUX_APP_ID'], ENV['FLUX_APP_SECRET']
end
```

You can now access the Flux OmniAuth URL: `/auth/flux`

### Devise OmniAuth

In `config/initializers/devise.rb`:

```ruby
Devise.setup do |config|
  # ...
  config.omniauth :flux, ENV['FLUX_APP_ID'], ENV['FLUX_APP_SECRET']
  # ...
end
```

In `models/user.rb`:

```ruby
class User < ActiveRecord::Base
  # ...
  devise :omniauthable, omniauth_providers: [:flux]
  # ...
end
```

**Note:** The devise line in `models/user.rb` will probably look different depending on what devise modules you are using.

## Turbolinks

Turbolinks can cause trouble, because the requests must all happen over https, and turbolinks jumps the gun somehow and makes an http request.

As such, disable turbolinks in your links like:

```ruby
<%= link_to user_omniauth_authorize_path(:flux), data: { no_turbolink: true } %>
```