# coding: utf-8
require File.expand_path(File.join('..', 'lib', 'omniauth', 'flux', 'version'), __FILE__)

Gem::Specification.new do |gem|
  gem.name          = 'omniauth-flux'
  gem.version       = OmniAuth::Flux::VERSION
  gem.authors       = ['Jami Couch']
  gem.email         = ['jami.couch@metova.com']
  gem.summary       = ''
  gem.description   = ''
  gem.homepage      = 'https://bitbucket.com/metova/omniauth-flux'
  gem.license       = 'MIT'

  gem.files         = `git ls-files -z`.split("\x0")
  gem.executables   = gem.files.grep(%r{^bin/}) { |f| File.basename(f) }
  gem.test_files    = gem.files.grep(%r{^(test|spec|features)/})
  gem.require_paths = ['lib']

  gem.add_runtime_dependency 'omniauth-oauth2', '~> 1.3.0'
  gem.add_runtime_dependency 'omniauth', '~> 1.2.0'

  gem.add_development_dependency 'rspec', '>= 2.14.0'
  gem.add_development_dependency 'rake'
  gem.add_development_dependency 'yarjuf'
  gem.add_development_dependency 'simplecov'

end
