require 'spec_helper'
require 'omniauth-flux'

describe OmniAuth::Strategies::Flux do
  let(:request) { double('Request', params: {}, cookies: {}, env: {}) }
  let(:app) {
    lambda do
      [200, {}, ["Hello."]]
    end
  }

  subject do
    OmniAuth::Strategies::Flux.new(app, 'appid', 'secret', @options || {}).tap do |strategy|
      allow(strategy).to receive(:request) {
        request
      }
    end
  end

  before do
    OmniAuth.config.test_mode = true
  end

  after do
    OmniAuth.config.test_mode = false
  end

  describe 'client_options' do
    it 'should have the correct site' do
      expect(subject.client.site).to eq 'https://id.fluxhq.io'
    end

    it 'should have the corrent authorize_url' do
      expect(subject.client.options[:authorize_url]).to eq '/oauth/authorize'
      expect(subject.client.authorize_url).to eq 'https://id.fluxhq.io/oauth/authorize'
    end
  end

  describe '#callback_path' do
    it 'should have the corrent callback path' do
      expect(subject.callback_path).to eq('/auth/flux/callback')
    end
  end

  describe '#raw_info' do
    let(:client) do
      OAuth2::Client.new('abc', 'def') do |builder|
        builder.request :url_encoded
        builder.adapter :test do |stub|
          stub.get('/api/v1/me.json') { |env| [200, {'content-type' => 'application/json'}, '{ "sub": "12345" }']}
        end
      end
    end

    let(:access_token) { OAuth2::AccessToken.from_hash(client, {}) }
    before do
      allow(subject).to receive(:access_token).and_return(access_token)
    end

    it 'should exist' do
      expect(subject.raw_info).to eq('sub' => '12345')
    end
  end

  describe '#info' do
    before { allow(subject).to receive(:raw_info).and_return({ 'name' => 'A Name', 'email' => 'a.name@email.com' }) }
    it 'should return the name and email' do
      expect(subject.info).to eq ({ name: 'A Name', email: 'a.name@email.com' })
    end
  end
end